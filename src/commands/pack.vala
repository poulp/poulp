using Poulp;

namespace Poulp.Commands {
    public class PackCommand : Object, Command {

        private Manifest manifest { get; set; }

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            try {
                this.manifest = Manifest.get_for_current_project ();
#if WIN32
                string cmd = "Compress-Archive -Path build -DestinationPath build\\bundle";
                string script_ext = "ps1";
#else
                string cmd = "zip build/bundle build/*";
                string script_ext = "sh";
#endif
                FileUtils.set_contents ("build/install.%s".printf (script_ext), install_script ());
                FileUtils.chmod ("build/install.%s".printf (script_ext), 777);
                int return_code = yield spawn_async (cmd);
                if (return_code == 0) {
                    Logger.logc (LogColor.GREEN, "✓ Bundle created!");
                }
                return return_code == 0;
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }
        }

        public string install_script () {
            if (this.manifest.scripts.has_key ("install")) {
                return string.joinv ("\n", this.manifest.scripts["install"].to_array ());
            } else {
                if (this.manifest.library) {
                    var ext = ".so";
#if WIN32
                    ext = ".dll";
#endif
                    return
"""
echo "Installing %s version %s"
cp *.vapi %s
cp *.deps %s
cp *.h %s
cp *.pc %s
cp *.%s %s
""".printf (this.manifest.name, this.manifest.version.to_string (),
            Environment.get_variable ("POULP_VAPI_DIR"),
            Environment.get_variable ("POULP_VAPI_DIR"),
            Environment.get_variable ("POULP_INCLUDE_DIR"),
            Environment.get_variable ("POULP_PKG_DIR"),
            ext, Environment.get_variable ("POULP_LIB_DIR"));
                } else {
                    return
"""
echo "Installing %s version %s"
cp ./* %s
""".printf (this.manifest.name, this.manifest.version.to_string (), Environment.get_variable ("POULP_BIN_DIR"));
                }
            }
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            if (!is_short) {
                return "Create an easily installable bundle for your project.";
            } else {
                return this.help (true) +
"""
To install it, just extract the files and run `install.sh` or `install.ps1`.
""";
            }
        }
    }
}
