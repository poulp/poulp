# How to contribute

Because poulp is libre software, everybody can help. But before coding anything, you have to know how do we work, to keep everything clean and organized.

## Before starting

- Make sure you have a Framagit account (if you don't want to create a new account, you can just login with GitHUb or GitLab.com) ;
- You have your own fork of poulp, and it's up-to-date ;
- You have an issue on which to work. If the issue doesn't exist yet, you can create it. Clearly says that you are working on it, so someone else will not start working on it too ;

## Making changes

For each issue you are solving, follow those instruction.

- Create a new branch named `issue-X` with `X` the ID of your issue (e.g. run `git checkout -b issue-42`). Most of the time, this new branch is based on master ;
- Start coding. Every time you finish something, even if it's small, commit it. You can push it to your fork if you want ;
- Be sure that you also updated the docs (`SPEC.md`) ;
- When you are ready, and if the build passes, create a merge request. It should have a title like that: `Fix #X : Description`. In merge request message, explain how to test what you have done ;
- Your merge request will be reviewed, and merged in `develop` !

Your commit messages should start with a (very) short description of what you have done. If you need more precision you should use a new line.
