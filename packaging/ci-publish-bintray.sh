###
# Poulp .rpm/.deb packages publisher (Bintray)
# This script is intended to be used only by gitlab-ci.
###
#!/bin/bash

RPM_FILE=$(ls | grep --color=never .rpm)
DEB_FILE=$(ls | grep --color=never .deb)
RPM_NAME=`rpm --queryformat "%{NAME}" -qp $RPM_FILE`
RPM_VERSION=`rpm --queryformat "%{VERSION}" -qp $RPM_FILE`
RPM_RELEASE=`rpm --queryformat "%{RELEASE}" -qp $RPM_FILE`
RPM_ARCH=`rpm --queryformat "%{ARCH}" -qp $RPM_FILE`

pwd
ls

echo "RPM_FILE = " $RPM_FILE
echo "DEB_FILE = " $DEB_FILE
echo "RPM_NAME = " $RPM_NAME
echo "RPM_VERSION = " $RPM_VERSION
echo "RPM_RELEASE = " $RPM_RELEASE
echo "RPM_ARCH = " $RPM_ARCH

function publish () {
  # Upload the .rpm to Bintray.
  curl -v --user $BINTRAY_USER:$BINTRAY_APIKEY \
    -H "Content-Type: application/json" \
    -X POST https://api.bintray.com/packages/$BINTRAY_ACCOUNT/rpm/$RPM_NAME/versions \
    --data "{ \"name\": \"$RPM_VERSION-$RPM_RELEASE\", \"desc\": \"New version, with improvements and bug fixes.\" }"

  curl -T $RPM_FILE --user $BINTRAY_USER:$BINTRAY_APIKEY \
    -H "X-Bintray-Package:$RPM_NAME" \
    -H "X-Bintray-Version:$RPM_VERSION-$RPM_RELEASE" \
    https://api.bintray.com/content/$BINTRAY_ACCOUNT/rpm/

  curl --user $BINTRAY_USER:$BINTRAY_APIKEY \
    -H "Content-Type: application/json" \
    -X POST https://api.bintray.com/content/$BINTRAY_ACCOUNT/rpm/$RPM_NAME/$RPM_VERSION-$RPM_RELEASE/publish \
    --data "{ \"discard\": \"false\" }"

  # Upload the .deb on Bintray.
  # Create a new version
  curl --user $BINTRAY_USER:$BINTRAY_APIKEY \
    -H "Content-Type: application/json" \
    -H "X-Bintray-Debian-Distribution: master" \
    -H "X-Bintray-Debian-Component: main" \
    -H "X-Bintray-Debian-Architecture: amd64" \
    -X POST https://api.bintray.com/packages/$BINTRAY_ACCOUNT/deb/$RPM_NAME/versions \
    --data "{ \"name\": \"$RPM_VERSION-$RPM_RELEASE\", \"desc\": \"New version, with improvements and bug fixes.\" }"

  # Upload the file
  curl -T $DEB_FILE --user $BINTRAY_USER:$BINTRAY_APIKEY \
    -H "X-Bintray-Package:$RPM_NAME" \
    -H "X-Bintray-Version:$RPM_VERSION-$RPM_RELEASE" \
    -H "Content-Type: application/json" \
    -H "X-Bintray-Debian-Distribution: master" \
    -H "X-Bintray-Debian-Component: main" \
    -H "X-Bintray-Debian-Architecture: amd64" \
    https://api.bintray.com/content/$BINTRAY_ACCOUNT/deb/

  # publish the version
  curl --user $BINTRAY_USER:$BINTRAY_APIKEY \
    -H "Content-Type: application/json" \
    -X POST https://api.bintray.com/content/$BINTRAY_ACCOUNT/deb/$RPM_NAME/$RPM_VERSION-$RPM_RELEASE/publish \
    --data "{ \"discard\": \"false\" }"
}

publish
