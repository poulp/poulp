using Gee;
using Poulp;

namespace Poulp.Commands {
    /**
    * Represent a project template.
    */
    public class ProjectTemplate : Object {

        /**
        * Creates a new project template from JSON.
        *
        * @param toml The JSON representation of this template
        */
        public ProjectTemplate (Json.Object json) {
            // we get data from the JSON to initialize the instance
            this.id = json.get_string_member ("id");
            this.name = json.get_string_member ("name");
            this.description = json.get_string_member ("description");
            foreach (var file in json["files"].as_array ()) {
                this.files.add (file.as<string> ());
            }
        }

        /**
        * The ID of this template
        */
        public string id { get; set; }

        /**
        * The name of this template.
        */
        public string name { get; set; }

        /**
        * A short description of what is this template.
        */
        public string description { get; set; }

        /**
        * The files in this template. They should be in a subfolder of poulp_dir/projects/ that is named with the ID of the template.
        */
        public ArrayList<string> files { get; set; default = new ArrayList<string> (); }

    }

    /**
    * A command that help you starting a new project
    */
    public class InitCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            // parsing arguments
            string name = null;
            string tmpl_arg = null;
            bool next_is_name = false, next_is_tmpl = false;
            foreach (string arg in args) {
                if (next_is_name) {
                    name = arg;
                    next_is_name = false;
                }

                if (next_is_tmpl) {
                    tmpl_arg = arg;
                    next_is_tmpl = false;
                }

                next_is_name = (arg == "-n" || arg == "--name");
                next_is_tmpl = (arg == "-t" || arg == "--template");
            }

            // we check if a poulp.toml file isn't already here
            if (FileUtils.test ("poulp.toml", FileTest.EXISTS)) { // If we already have a package file, warn the user.
                Logger.logc (LogColor.BOLD_YELLOW, "A poulp.toml file already exists for this folder. It will be overriden.");
                bool proceed = Logger.askyn ("Are you sure you want to proceed anyway?");
                if (proceed == false) {
                    Logger.logc (LogColor.RED, "Project creation aborted.");
                    return false;
                }
                print ("\n");
            }

            Logger.logc (LogColor.BOLD_BLUE, "# Creating a new Vala project");

            Manifest man = new Manifest.custom ("my-project"); // the manifest of the new project. It will be writen in `poulp.toml` at the end of this method
            ProjectTemplate template = null;

            // Ask for project name.
            if (name == null) {
                string proj_name = Logger.ask ("- Project name: ");
                man = new Manifest.custom (proj_name);
            } else {
                man = new Manifest.custom (name);
            }

            // Ask for project version.
            string proj_version = Logger.ask ("- Project version (0.1.0): ");
            if (proj_version != "" && SemVer.valid (proj_version)) {
                man.version = new SemVer.Version.parse (proj_version);
            } else {
                man.version = new SemVer.Version (0, 1, 0);
            }

            // Ask for project author.
            man.authors.add (Logger.ask ("- Project author (Full name <mail@domain.tld>): "));

            // Ask for project license.
            string proj_license = Logger.ask ("- Project license (MIT): ");
            if (proj_license != "") {
                man.license = proj_license;
            } else {
                man.license = "MIT";
            }

            // Ask for project repository.
            string git_repo = Logger.ask ("- Git repository: ");
            if (git_repo != "") {
                man.repository = git_repo;
            }

            // template
            var prjs = new ArrayList<ProjectTemplate> ();

            // we read the templates from `projects.toml` and we display them
            try {
                string projects_path = Path.build_filename (get_poulp_dir (), "projects", "projects.json");
                string projects;
                FileUtils.get_contents (projects_path, out projects);
                Json.Parser parser = new Json.Parser ();
                parser.load_from_data (projects);
                foreach (var elt in parser.root.as_array ()) {
                    // ad the template to the list
                    ProjectTemplate tmpl = new ProjectTemplate (elt.as_object ());
                    prjs.add (tmpl);
                };
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }

            if (tmpl_arg != null) {
                // try to get a number
                int64 id;
                if (int64.try_parse (tmpl_arg, out id)) {
                    template = prjs[(int) id];
                }

                // else if the input is a string...
                foreach (ProjectTemplate prj in prjs) {
                    // we try to find a template with the corresponding name or ID
                    if (prj.id == tmpl_arg || prj.name.down () == tmpl_arg.down ()) {
                        template = prj;
                    }
                }
            } else {
                Logger.logc (LogColor.BOLD_BLUE, "\n# Template");
                bool use_template = Logger.askyn ("- Would you like to use a template ?");
                if (use_template) {
                    // the user wants to use a template
                    bool valid_choice = false;

                    while (!valid_choice) {
                        // while the selected template isn't in the list
                        print ("Which template would you like to use?\n");
                        print ("(Enter a number or the ID. If you finally don't want a template, just press Enter)\n\n");

                        int index = 0;
                        foreach (ProjectTemplate prj in prjs) {
                            print ("%d - %s - %s: %s \n", (int)index, prj.id, prj.name, prj.description);
                            index++;
                        }

                        string tmpl_id = stdin.read_line ();

                        // try to get a number
                        int64 id;
                        if (int64.try_parse (tmpl_id, out id)) {
                            template = prjs[(int) id];
                        }

                        // else if the input is a string...
                        foreach (ProjectTemplate prj in prjs) {
                            // we try to find a template with the corresponding name or ID
                            if (prj.id == tmpl_id || prj.name.down () == tmpl_id.down ()) {
                                template = prj;
                            }
                        }

                        valid_choice = template != null || tmpl_id == ""; // valid choice
                    }
                }
            }

            // we copy data from the `poulp.toml` of the template if needed.
            if (template != null) {
                try {
                    string tmpl_dir = Path.build_filename (get_poulp_dir (), "projects", template.id);
                    Manifest tmpl_man = new Manifest.for_path (Path.build_filename (tmpl_dir, "poulp.toml"));
                    man.dependencies = tmpl_man.dependencies;
                    man.keywords = tmpl_man.keywords;
                } catch (Error err) {
                    Logger.log_error (err.message);
                }
            }

            // Check if the project already contains source files, if so add them to man.files
            man.add_files (Environment.get_current_dir ());

            // Let's generate the project toml.
            string toml = man.to_toml_string ();

            Logger.logc (LogColor.BOLD_BLUE, "\n# Project overview: ");
            Logger.log ("The following poulp.toml will be generated: ");
            Logger.log (toml);
            if (man.repository != null) {
                Logger.log ("A new Git repository will also be created.");
            }

            // Ask the user whether or not it wants to write this file to disk.
            bool proceed = Logger.askyn ("\nAre you sure you want to proceed anyway?");
            if (proceed) {
                try {
                    // copy files from template
                    if (template != null) {
                        foreach (string _file in template.files) {
                            string file = Path.build_filename (get_poulp_dir (), "projects", template.id, _file);
                            print ("Copying %s from the %s template...", file, template.id);
                            File src = File.new_for_path (file);
                            string dest_path = Path.build_filename (Environment.get_current_dir (), _file);
                            File dest = File.new_for_path (dest_path);
                            src.copy (dest, FileCopyFlags.OVERWRITE, null, null);
                            print (" [OK]\n");
                        }
                    }
                    // write `poulp.toml`
                    FileUtils.set_contents (Path.build_filename (Environment.get_current_dir (), "poulp.toml"), toml);

                    yield spawn_async ("git init");
                    if (man.repository != null) {
                        yield spawn_async (@"git remote add origin $(man.repository)");
                    }

                    Logger.logc (LogColor.GREEN, "Project `%s` has been initialized.", man.name);
                    Logger.logc (
                        LogColor.BLUE, "Type %s%s%s to build.",
                        LogColor.YELLOW, "poulp build", LogColor.BLUE
                    );
                } catch (Error err) {
                    Logger.log_error ("Generating poulp.toml failed. Error: %s", err.message);
                }
            } else {
                Logger.logc (LogColor.BOLD_RED, "Project creation aborted.");
                return false;
            }

            return true;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            string res = "This little tool help you quickly creating a new project.";
            if (!is_short) {
                res += "\n\nIt looks for templates in " + Path.build_filename (get_poulp_dir (), "projects") + ".";
            }
            return res;
        }
    }
}
