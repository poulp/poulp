using Poulp;
using Gee;

namespace Poulp.Commands {

    /**
    * A command that displays help about other commands
    */
    public class HelpCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            // if there is no specific argument, we just show a list of available commands
            if (args.length == 0) {
                Logger.log ("You can run any of those commands with poulp");
                foreach (var cmd in this.commands.entries) {
                    Logger.log ("  - %s: %s", cmd.key, cmd.value.help (true));
                }
                Logger.log ("\nTo get help about a specific command, use 'poulp help [command]'\n");
            } else { // else we display the help about the command
                if (this.commands.has_key (args[0])) {
                    Logger.log (this.commands [args[0]].help (false));
                } else {
                    Logger.log_error ("It looks like the %s command isn't available.",args[0] );
                }
            }
            return true;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short = true) {
            if (is_short) {
                return "Displays a help message about poulp or a specific command.";
            } else {
                return "You really tried to get help about the help?";
            }
        }

        /**
        * The commands known by poulp
        */
        public HashMap<string, Command> commands { get; set; }
    }
}
