using Gee;
using Poulp.Commands;

namespace Poulp {

    public MainLoop loop;

    /**
    * The application entry-point.
    *
    * It registers commands and execute them if possible.
    */
    int main (string[] args) {
        setup_env ();
        if (!create_directories ("--reinit" in args)) {
            return 1;
        }

        if (args.length < 2) {
            Logger.log ("Usage: %s [command] <args>", args[0]);
            Logger.log ("Try %s help to see a list of all the avalaible commands.", args[0]);
            return 1;
        }

        loop = new MainLoop ();

        // the commands registry
        HashMap<string, Command> commands = new HashMap<string, Command> ();
        commands["init"] = new InitCommand ();
        commands["build"] = new BuildCommand ();
        commands["install"] = new InstallCommand ();
        commands["repo"] = new RepositoryCommand ();
        commands["run"] = new RunCommand ();
        commands["lint"] = new LintCommand ();
        commands["gen"] = new GenCommand ();
        commands["update"] = new UpdateCommand ();
        commands["publish"] = new PublishCommand ();
        commands["pack"] = new PackCommand ();
        commands["help"] = new HelpCommand () {
            commands = commands
        };

        if (commands.has_key (args[1])) {
            bool result = true;
            commands[args[1]].run.begin (args[2:args.length], (obj, res) => {
                result = commands[args[1]].run.end (res);
                loop.quit ();
            });
            loop.run ();
            return result ? 0 : 1;
        } else {
            Logger.log_error ("Unknown command: %s", args[1]);
            return 1;
        }
    }
}
