# Poulp

Poulp is like npm, but for Vala.

It is a little programm that'll help you generating files or projects, compiling your app, managing your dependencies and more.

## Usage

First, create a `poulp.toml` file. You can also use `poulp init` to do it.

```toml
name = "my-app"
version = "1.42.0"

[dependencies]
"gtk+3.0" = "*"
"gio-2.0" = "*"
"libsoup-2.4" = "*"
semver = "^0.2.0"
```

Then, open a terminal and just run the following command.

```
poulp build
```

It will run this command for you.

```
valac *.vala -o build/my-app --pkg gtk+-3.0 --pkg gio-2.0 --pkg libsoup-2.4 --pkg semver
```

If you need some libraries that are not installed, run `poulp install`. With the manifest above, it will probably install a matching version of [`semver`](https://framagit.org/Bat/semver).
When installing a library, it will search for it in remote repositories, clone the matching git repository, compile it for your system and install it.

For more informations, please refer to [the website](https://poulp.frama.io).

# Hosting libraries

You can see an exemple repo here: https://gitlab.com/Bat41/poulp-repo-test/tree/master/.

You can use it with poulp by doing `poulp repo add https://gitlab.com/Bat41/poulp-repo-test/raw/master/dist`.
