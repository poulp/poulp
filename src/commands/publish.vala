using Poulp;
using SemVer;

namespace Poulp.Commands {

    /**
    * Publishes a new version of a package
    */
    public class PublishCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            string version_type = "patch";
            if (args[0] in new string [] { "major", "minor", "patch" }) {
                version_type = args[0];
            }

            try {
                Manifest man = Manifest.get_for_current_project ();

                switch (version_type) {
                    case "major":
                        man.version.publish (VersionType.MAJOR);
                        break;
                    case "minor":
                        man.version.publish (VersionType.MINOR);
                        break;
                    case "patch":
                        man.version.publish (VersionType.PATCH);
                        break;
                }

                FileUtils.set_contents ("poulp.toml", man.to_toml_string () + "\n");

                yield spawn_async ("git add poulp.toml");
                yield spawn_async (@"git commit -m '$(man.version) release' -q");
                yield spawn_async (@"git tag -m 'Version $(man.version)' $(man.version)");
                yield spawn_async ("git push poulp master --follow-tags");
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }

            return true;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool short_version) {
            if (short_version) {
                return "Publish a new version of a package";
            } else {
                return this.help (true) +
"""Specify the type of release it is (major, minor or patch) and enter your Git credentials: your new version is ready to be installed.
But you should also be sure that your package is registered in a repository.""";
            }
        }

    }
}
