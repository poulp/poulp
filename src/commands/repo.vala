using Poulp;
using Gee;

namespace Poulp.Commands {

    /**
    * A command to manage your distant repositories (add, remove or list them).
    */
    public class RepositoryCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            try {
                // reads repo list
                ArrayList<string> repos = new ArrayList<string> ();
                string repos_file = Path.build_filename (get_poulp_dir (), "repos");
                string repo_list;
                FileUtils.get_contents (repos_file, out repo_list);
                foreach (string line in repo_list.split ("\n")) {
                    if (line.length > 0) {
                        repos.add (line);
                    }
                }
                // if we do `poulp repo` or `poulp repo list`, we just list the repositories
                if (args.length == 0 || args[0] == "list") {
                    foreach (string repo in repos) {
                        print ("  - %s\n", repo);
                    }
                } else { // else we can add or remove a repository
                    switch (args[0]) {
                        case "add":
                            repos.add (args[1]);
                        break;
                        case "remove":
                            repos.remove (args[1]);
                        break;
                        default:
                            Logger.log_warning ("Unknown command: %s", args[0]);
                        break;
                    }

                    // finally, we write the new repo list to a file
                    string content = "";
                    foreach (string repo in repos) {
                        content += repo + "\n";
                    }
                    FileUtils.set_contents (repos_file, content);
                }
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }

            return true;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            if (is_short) {
                return "Manage your distant repositories";
            } else {
                return """Manage your distant repositories.
You can use `add <url>` to add a new repository, `remove <url>` to remove one, and `list` to list them.""";
            }
        }
    }
}
