using Poulp;

namespace Poulp.Commands {
    /**
    * A command that runs a script defined in the `poulp.toml` file of the project.
    */
    public class RunCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            try {
                if (args.length == 0) {
                    Logger.log_error ("No script specified. Please specify a script.");
                    return false;
                }

                Manifest man = Manifest.get_for_current_project ();

                foreach (string arg in args) { // for each script to run
                    if (man.scripts.has_key (arg)) { // if it exists
                        foreach (string cmd in man.scripts [arg]) { // we just run every command
                            int exit_code = yield spawn_async (cmd);

                            // we show a little message that say that evrything is OK (or not)
                            if (exit_code != 0) {
                                Logger.logc (LogColor.BOLD_RED, "⨯ \"%s\": command '%s' exited with %d", arg, cmd, exit_code);
                                return false;
                            }
                        }
                        Logger.logc (LogColor.BOLD_GREEN, "✓ \"%s\"", arg);
                    } else {
                        Logger.log_warning ("Can't find the script named '%s'.", arg);
                    }
                }
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }
            return true;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            if (is_short) {
                return "Runs a script defined in the manifest";
            } else {
                return "Runs a script defined in the manifest. You can run multiple script using `poulp run script1 script2 script3`";
            }
        }

    }
}
