# poulp's specification

*This document aims to provide a description of how poulp will work when it will be production-ready.*

## poulp.toml

The `poulp.toml` file contains some [TOML](https://github.com/toml-lang/toml) defining the properties of a project. It is located at the root of the project.

It can contains the following keys.

### name

The name of the project. It should only contain (small) letters, numbers or hyphens, and it must start with a letter.

```toml
name = "my-project" # correct
name = "my-project-123" # correct

name = "GTK+ App" # Incorrect : capitals, +, and space
name = "42-the-meaning-of-life-etc" # Incorrect : begins with a number
```

*This key is mandatory.*

### version

This is the version of your package. It should be a valid [semantic version](http://semver.org).

*This key is mandatory.*

### description

This is just a short description of your package.

### keywords

An array containing strings, which are the keywords of your package. It help people finding them when doing `poulp search`, and on the repositories websites using [poulp server](https://framagit.org/poulp/poulp-server).

### homepage

This is a string containing the URL of the projects website.

### bugs

Specifies where to report bugs. It is an table, which can contain this fields.

- `url`, the bug tracker URL ;
- `email`, an email adress where we can send bug reports.

### license

The name of the license of your project, or the path to the file containing its license.

### authors

The names of the authors of the project. They could contain an email adress after the name, between `<` `>`.

### files

The files to include at compilation.

### repository

The URL of the projects repository, where we can find the source code. It will be used when running `poulp publish`.

### vapi-dir

The path to a directory containing personal VAPIs.

### output

Where to put the result of the compilation. Defaults to `build/name-version` (where `name` is the name of the project and `version` its version).

### library

A boolean that tells us if this project is a library or not. If it is, it will be compiled in a special way, to make it reusable by everyone.

### scripts

Some custom scripts, that can be run whit `poulp run`. It's a table containing the fields you want (their name will be used as the argument of `poulp run`). Each of those fields are arrays with the list of the command to run.

### dependencies

The projects dependencies. It is an object where the keys are the name of the dependencies, and the value a range of versions. The name should be the same as the VAPI file name.

To see examples of version ranges, go to [semver](https://framagit.org/Bat/semver).

### dev-dependencies

Same as `dependencies`, but will only be installed by people developing the project. For instance, it could a testing framework.

### vala-version

The Vala version to use with this project. It is also a semantic versionning range.

### features

Compiler features that can be enabled or disabled. This table contains only booleans (`true` means enables, `false` disabled). Available features are:

- fast-vapi, to generate fast `.vapi` files, without "absolute" symbols (e.g `using Gtk;` and `Button` instead of `Gtk.Button`)
- vapi-comments, to keep comments in `.vapi`
- debug, to make your project easily debuggable with `gdb`
- thread, to enable multithreading support in the compiler
- memory-profiler, to enable GLib memory profiler
- standard-packages, to disable packaging `glib-2.0`, `gobject-2.0` and `gmodule-2.0`
- assert, to disable `assert`
- checking, to enable `checking`
- deprecated, to allow the `[Deprecated]` attribute
- hide-internal, to mark `internal` symbols as `private` in C code (`internal` doesn't exist in C)
- experimental, to enable experimental features
- warnings, to turn off warnings
- fatal-warnings, if you want warnings to make your build failing
- since-check, to avoid checking if the symbol exists in local packages
- non-null, to ensure that nothing is never `null`
- gobject-tracing, to turn on GObject tracing
- version-header, if you want to write the valac version in the `.h` file
- c-warnings, to turn off c warnings

### compiler-params

Some other arguments to gives to `valac` when building.




## Commands

Each command listed here can be run with `poulp command`, where `command` is the command followed by its arguments.

`[arg]` is an optional argument.

### search package

Search the package named `package` in the known repositories. It will display something like that.

```
package    0.1.0    Short description of the package.
other      0.2.3    Yet another package.
```

### info package

Gives infos about `package`.

```
✓ Installed

Name          package
Version       0.1.0 (latest is 0.2.0, update with `poulp update package`)
Description   This is a package.
Author        John Doe
Contributors  Jane Doe, Foo Bar

Files         package.vapi, package.h, package.so, package.pc
Dependencies  other (0.2.3), foo (<2.1.0), bar (*)
Website       https://john.doe.eu/projects/package
Source code   https://john.doe.eu/git/package.git
```

### pack [--zip|--gz]

Creates a .zip or .tar.gz file containing all the binaries of the project (.h, .vapi, .so, .dll, etc)

### publish major|minor|patch --repo url [--no-changelog]

Pack the project (see `pack` above), creates a new version (update it in the manifest, `git tag` if the project uses `git`),
and update the changelog with `git log`, if it was not manually done (this last behavior can be disabled with `--no-changelog`).

Then it post the new version on the repository at `url`.

**Warning** `--repo` and `--no-changelog` are not implemented yet.

### build [--release]

Compiles your project using the options specified in the manifest.

### gen output --template template

Creates a new file (`output`) from the template named `template`. `output` doesn't need to include any extension.

### help [command]

Gives you help about poulp, or a specific command if `command` is specified.

### init

An interactive tool to create a new project.

### install [lib] [lib] [lib]...

Install all the specified libraries, or those present in `libs` in the manifest if none.

It clones the libraries, checkout to the most recent matching version, builds it and installs the result.

### lint [--elementary|--gnome|--both]

Lints your code, following the specified convention (defautl is elementary).

### repo [add|remove] [url]

Manages your remote repositories. If there is no argument, it just lists them.

If you do [add url], it registers the repo at `url`. And `remove url` unregisters the repository at `url`.

### run script

It runs the script named `script`, previousy defined in the manifest.

### update [lib] [lib] [lib]...

Updates your packages to the latest version.




# Variables

poulp defines some environment varaibles when spawning a command. You can use them in `output`, `compiler-params` and your scripts.

- `$PROJECT_NAME`, the name of your project;
- `$PROJECT_VERSION`, the version of your project;
- `$PROJECT_DESCRIPTION`, the description of your project;
- `$PROJECT_LICENSE`, the license of your project;
- `$PROJECT_HOMEPAGE`, the homepage of your project;
- `$PROJECT_OUTPUT`, the path to the output of your project;

# Remote repositories organization

*You can find a sample repository [here](https://gitlab.com/Bat41/poulp-repo-test/tree/master/dist).*

The remote repositories just contains one file, named `packages.json`. It contains a list of all the packages available in this repository. It also contains another field, `name`, which is the name of the repository.

This file has an object, named `packages` which contains key-value pairs of packages (the name is the key, and the git repository the value).

*Example `packages.json` file*

```
{
  "name": "A little poulp repository",
  "packages": {
    "foo": "https://git.foo.org/foo.git",
    "bar": "https://github.com/baz/bar",
    "hello": "https://gitlab.com/hey/hello"
  }
}
```

It is planed to make it possible for package to install templates, but not available yet.

----

Because it could be a little bit hard to manually manage a repository, we are [working on a web interface](https://framagit.org/valse/poulp-server) to do it easily. But yet, you can just use a git repository.



# Local organization

poulp stores local files in `$POULP_DATA_DIR`, which defaults to `C:\ProgramData\poulp\` on Windows and `/etc/poulp/` on other systems.

This directory contains a file named `repos`, which contains a list of all known repos (their URL to be exact), one per line.

Then, you have the `projects` and `files` folders, similar to those in a distant repository, but only with installed templates.
