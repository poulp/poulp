using SemVer;
using Poulp;
using Gee;

namespace Poulp.Commands {

    /**
    * A command to install packages from a distant repositories
    */
    public class InstallCommand : Object, Command {

        /**
        * Used to install packages
        */
        private Installer installer { get; set; }

        /**
        * Initialize the {@link Poulp.Installer}
        */
        public InstallCommand () {
            this.installer = new Installer ();
            this.installer.not_found.connect ((pack) => {
                Logger.log_error (@"Can't find any package named $pack");
            });
        }

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            // We wreate a list of packages to install, by filtering args
            ArrayList<string> pkgs = new ArrayList<string> ();
            foreach (string arg in args) {
                if (!arg.has_prefix ("-")) {
                    pkgs.add (arg);
                }
            }
            if (pkgs.size == 0) { // no packages -> we use those in the `poulp.toml`
                try {
                    bool res = true;
                    foreach (var dep in Manifest.get_for_current_project ().dependencies.entries) {
                        res = res && yield install (dep.key, dep.value,
                            "--force" in args,
                            "-v" in args || "--verbose" in args,
                            "--no-templates" in args);
                    }
                    return res;
                } catch (Error err) {
                    Logger.log_error (err.message);
                    return false;
                }
            } else {
                bool res = true;
                foreach (string pkg in pkgs) {
                    res = res && yield install (pkg, new Range ("*"),
                        "--force" in args,
                        "-v" in args || "--verbose" in args,
                        "--no-templates" in args);
                }
                return res;
            }
        }

        /**
        * Installs a package
        *
        * @param pkg The name of the package to install
        * @param required The {@link SemVer.Range} that should contain a version of the package.
        * @param force true if we should reinstall the package
        * @param verbose true if we want a verbose output
        * @param no_templates true if we don't want to install templates. Useless, because we don't have templates anymore (they'll be back in a future version)
        */
        private async bool install (string pkg, Range required, bool force, bool verbose, bool no_templates) {
            try {
                string pc_stdout;
                int pc_status;
                // checking for package with pkg-config
                Process.spawn_command_line_sync ("pkg-config --modversion " + pkg, out pc_stdout, null, out pc_status);
                if (pc_status == 0) {
                    if (valid (pc_stdout.replace ("\n", ""))) {
                        var v = new SemVer.Version.parse (pc_stdout.replace ("\n", ""));
                        if (v in required) {
                            Logger.logc (LogColor.BOLD_GREEN, @"✓ $pkg already installed.");
                            return true; // a good version of the package is already installed
                        } else {
                            Logger.logc (LogColor.BLUE, @"$pkg is already installed, but $v is not in $required. Another version will be installed.");
                        }
                    } else {
                        if (force) {
                            Logger.log_warning (@"$pkg is not using semantic versionning. But we will assume that it's OK.");
                        } else {
                            Logger.log_error (@"$pkg is not using semantic versionning. If you really want to install this package, use '--force`, but you could have problems in the future...");
                            return false;
                        }
                    }
                }
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }

            // fetching repos if needed
            if (this.installer.repositories.size == 0) {
                Logger.logc (LogColor.BLUE, "Fetching data from remote repositories... Please wait, this could take a while.");
                try {
                    this.installer.repositories = yield Repository.load_known ();
                } catch (Error err) {
                    Logger.log_error (err.message);
                    return false;
                }
            }

            // we install the package
            bool success = yield this.installer.install (pkg, required);

            if (success) {
                Logger.logc (LogColor.BOLD_GREEN, @"✓ Successfuly installed $pkg.");
            }
            return true;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            if (is_short) {
                return "Install a given package or all the packages specified in the manifest.";
            } else {
                return """Install a given package or all the packages specified in the manifest.
If no packages can be found, check that you registered the repository with `poulp repo add`.""";
            }
        }
    }
}
