###
# Poulp .deb package maker.
# This script is intended to be used only by gitlab-ci.
###
#!/bin/bash

function before_script () {
  apt-get update
  apt-get install -y ruby ruby-dev dpkg-sig libffi-dev gcc make git
  gem install fpm

  export POULP_VERSION=$(git describe --tags --abbrev=0)
  if [ -z "$POULP_VERSION" ]; then
    export POULP_VERSION=$(git rev-parse --short HEAD)
  fi
}

function create_package () {
  fpm --provides poulp \
    --replaces poulp \
    --depends vala --depends libgee \
    --license "LGPLv3" \
    --url $CI_PROJECT_URL \
    --description "Vala projects and packages manager" \
    -a $(uname -m) \
    -C ./build/ \
    -s dir -t deb -f \
    -p poulp-VERSION.ARCH.deb \
    -n poulp \
    -v $POULP_VERSION \
    ./poulp=/usr/bin/poulp
}

function main () {
  before_script
  create_package
  exit 0
}

main
