using SemVer;
using Gee;
using Poulp;

namespace Poulp.Commands {

    /**
    * A command to update your packages
    */
    public class UpdateCommand : Object, Command {

        /**
        * Used to install new version of packages
        */
        private Installer installer { get; set; }

        /**
        * Initialize the {@link Poulp.Installer}
        */
        public UpdateCommand () {
            this.installer = new Installer ();
            this.installer.not_found.connect ((pack) => {
                Logger.log_error (@"Can't find any package named $pack");
            });
        }

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            // We wreate a list of packages to install, by filtering args
            ArrayList<string> pkgs = new ArrayList<string> ();
            foreach (string arg in args) {
                if (!arg.has_prefix ("-")) {
                    pkgs.add (arg);
                }
            }
            if (pkgs.size == 0) { // no packages -> we use those in the `poulp.toml`
                try {
                    bool res = true;
                    foreach (var dep in Manifest.get_for_current_project ().dependencies.entries) {
                        res = res && yield this.update (dep.key, dep.value,
                            "--force" in args,
                            "-v" in args || "--verbose" in args,
                            "--no-templates" in args);
                    }
                    return res;
                } catch (Error err) {
                    Logger.log_error (err.message);
                    return false;
                }
            } else {
                bool res = true;
                foreach (string pkg in pkgs) {
                    res = res && yield this.update (pkg, new Range ("*"),
                        "--force" in args,
                        "-v" in args || "--verbose" in args,
                        "--no-templates" in args);
                }
                return res;
            }
        }

        /**
        * Updates a package
        *
        * @param pkg The name of the package to update
        * @param required The {@link SemVer.Range} in which the new version should be
        * @param force true if we should reinstall the package
        * @param verbose true if we want a verbose output
        * @param no_templates true if we don't want to install templates. Useless, because we don't have templates anymore (they'll be back in a future version)
        */
        private async bool update (string pkg, Range required, bool force, bool verbose, bool no_templates) {
            try {
                int pkg_exists;
                Process.spawn_command_line_sync (@"pkg-config --exists $pkg", null, null, out pkg_exists);
                if (pkg_exists != 0) {
                    Logger.log_error (@"The package $pkg isn't installed on your computer.");
                    return false;
                }

                if (this.installer.repositories.size == 0) {
                    Logger.logc (LogColor.BLUE, "Fetching data from remote repositories... Please wait, this could take a while.");
                    this.installer.repositories = yield Repository.load_known ();
                }

                var installed = this.get_installed_version (pkg);
                var latest = this.get_last_matching_version (pkg, required);
                if (installed.eq (latest)) {
                    Logger.logc (LogColor.BOLD_GREEN, @"✓ $pkg is already up-to-date.");
                    return true;
                } else if (installed.gt (latest)) {
                    Logger.logc (LogColor.BOLD_GREEN, @"✓ I don't know how it is possible, but the current $pkg version is newer than the latest published version.\nI will not update it.");
                    return true;
                }

                bool success = yield this.installer.install (pkg, new Range (latest.to_string ()));

                if (success) {
                    Logger.logc (LogColor.BOLD_GREEN, @"✓ Successfuly updated $pkg.");
                    return true;
                }
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }
            return true;
        }

        /**
        * Get the installed version of a package
        *
        * Fallbacks to 0.0.0
        *
        * @param name The name of the package you want to know the installed version
        */
        public SemVer.Version get_installed_version (string name) {
            try {
                string pc_stdout;
                int pc_status;

                Process.spawn_command_line_sync ("pkg-config --modversion " + name, out pc_stdout, null, out pc_status);
                if (pc_status == 0 & valid (pc_stdout)) {
                    return new SemVer.Version.parse (pc_stdout);
                }
            } catch {
                return new SemVer.Version (0, 0, 0);
            }
            return new SemVer.Version (0, 0, 0);
        }

        /**
        * Return the last available {@link SemVer.Version} of a package, matching a range.
        *
        * @param name The name of the package
        * @param rng The range in which the version should be.
        */
        public SemVer.Version get_last_matching_version (string name, Range rng) throws PoulpError {
            string remote_url = null;
            foreach (var repo in this.installer.repositories) {
                foreach (var package in repo.packages.entries) {
                    if (package.key == name) {
                        remote_url = package.value;
                    }
                }
                if (remote_url != null) {
                    break;
                }
            }

            if (remote_url == null) {
                throw new PoulpError.PACKAGE_NOT_FOUND (@"Can't find any package named $name.");
            }

            try {
                string tags_list;
                int git_exit_code;
                Process.spawn_command_line_sync (@"git ls-remote --tags $remote_url", out tags_list, null, out git_exit_code);
                if (git_exit_code == 0) {
                    Regex tags_re = /^\w+\s+refs\/tags\/(?<tag>[\w.]+)$/;
                    var latest = new SemVer.Version (0, 0, 0);
                    foreach (string line in tags_list.split ("\n")) {
                        MatchInfo info;
                        if (tags_re.match (line, 0, out info)) {
                            string raw_ver = info.fetch_named ("tag");
                            if (valid (raw_ver)) {
                                var ver = new SemVer.Version.parse (raw_ver);
                                latest = ver.gt (latest) && ver in rng ? ver : latest;
                            }
                        }
                    }
                }
            } catch (Error err) {
                Logger.log_error (err.message);
            }
            return new SemVer.Version (0, 0, 0);
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            if (is_short) {
                return "Updates your packages";
            } else {
                return
"""Updates your packages, but only if a newer version is available and if it still matches the pattern defined in the manifest.
If packages to update are specified from the command line, the pattern we use is '*`.""";
            }
        }
    }
}
