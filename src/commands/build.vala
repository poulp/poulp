using Gee;
using Poulp;

namespace Poulp.Commands {

    /**
    * A command that build the current project
    *
    * It uses the different properties from the manifest.
    */
    public class BuildCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            try {
                Builder bld = new Builder (Manifest.get_for_current_project ());
                BuildOutput build = yield bld.build (Environment.get_current_dir (), "--release" in args);
                if ("--install" in args) {
                    return yield build.install ();
                }
                return build.success;
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short = true) {
            if (is_short) {
                return "Builds your project.";
            } else {
                return """Builds your project, using the informations in poulp.toml.

Use --release to build your project without packaging dev-dependencies.
Use --install to install the generated output on your computer (it may require root privileges).
""";
            }
        }
    }
}
