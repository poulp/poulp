using Poulp;
using Gee;

namespace Poulp.Commands {

    /**
    * A command tht lints your code, followin elementary's guidelines or GNOME ones.
    */
    public class LintCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            try {
                foreach (string file in recursive_ls (Environment.get_current_dir ())) { // foreach file of the project
                    if (file.has_suffix (".vala")) {
                        string src;
                        FileUtils.get_contents (file, out src);
                        ArrayList<string> errors = new ArrayList<string> ();

                        // selecting the good linter and lint the code
                        if ("--gnome" in args) {
                            errors = gnome_lint (src);
                        } else if ("--both" in args) {
                            errors = gnome_lint (src);
                            errors.add_all (elementary_lint (src, "--ignore-properties" in args));
                        } else {
                            errors = elementary_lint (src, "--ignore-properties" in args); // defaults to elementarys one
                        }

                        // logging errors if there are some
                        if (errors.size == 0) {
                            Logger.logc (LogColor.GREEN, @"✓ No problems in $file");
                        } else {
                            Logger.logc (LogColor.RED, @"⨯ $(errors.size) problem$(errors.size == 1 ? " " : "s ")in $file :");
                            foreach (string err in errors) {
                                Logger.logc (LogColor.RED, @"  - $err");
                            }
                            if ("--fatal" in args) {
                                return false;
                            }
                        }
                    }
                }
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }

            return true;
        }

        /**
        * Lint some code using elementary OS guidelines.
        *
        * Some guidelines are not yet implemented. A full list can be found here: https://elementary.io/docs/code/reference#code-style
        *
        * @param source The code to lint
        * @param ignore_props Because there isn't always a property corresponding to a set_* or a get_* method, we can disable this rule to avoid fake errors.
        * @return An {@link Gee.ArrayList} of {@link GLib.string} containing the errors
        */
        private ArrayList<string> elementary_lint (string source, bool ignore_props = false) {
            ArrayList<string> errs = new ArrayList<string> ();
            try {

                Regex par_re = /\w\(/; // to match parentheses with a non-empty character before
                Regex math_re = /(\w[+=\-*\/]|[+=\-*\/]\w|\w[+=\-*\/]\w)/; // to match math symbols that are not well spaced

                bool after_meth_declaration = false; // true if we are on the line after the declaration of a method

                int line_num = 0; // the number of the current line, useless now

                // to ignore multiline comments
                bool in_multiline_comment = false;
                Regex mlc_start_re = /\/\*.*$/;
                Regex mlc_end_re = /^.*\*\//;

                foreach (string line in source.replace ("\r", "").split ("\n")) { // for each line of the file
                    line_num++;
                    // remove string and comments
                    line = (/ ?\/\/.*$/).replace (line, line.length, 0, "");
                    line = /".*?"/.replace (line, line.length, 0, "");

                    if (mlc_start_re.match (line)) {
                        line = mlc_start_re.replace (line, line.length, 0, "");
                        in_multiline_comment = true;
                    } else if (mlc_end_re.match (line)) {
                        line = mlc_end_re.replace (line, line.length, 0, "");
                        in_multiline_comment = false;
                    }

                    if (in_multiline_comment) {
                        continue;
                    }

                    // FIXME
                    if (line._chug ().length == line.length - 2 || "\t" in line) {
                        errs.add (@"Line $line_num : Use 4 spaces for indentation");
                    }

                    if (line.length > 120) {
                        errs.add (@"Line $line_num : Maximum line width is 120 characters. You have $(line.length) characters.");
                    }

                    if (line.has_suffix (" ")) {
                        errs.add (@"Line $line_num : Trailing spaces at the end of the line");
                    }

                    if (par_re.match (line)) {
                        errs.add (@"Line $line_num : Missing space before parentheses");
                    }

                    if (" as " in line) {
                        errs.add (@"Line $line_num : Avoid using `as'");
                    }

                    if (!ignore_props) {
                        if (".get_" in line) {
                            errs.add (@"Line $line_num : Prefer `x.property' over `x.get_property ()' if possible");
                        }
                        if (".set_" in line) {
                            errs.add (@"Line $line_num : Prefer `x.property = y' over `x.set_property (y)' if possible");
                        }

                        if (".get (" in line || ".get(" in line) {
                            errs.add (@"Line $line_num : Prefer `x[y]' over `x.get (y)'");
                        }
                        if (".set (" in line || ".set(" in line) {
                            errs.add (@"Line $line_num : Prefer `x[y] = z' over `x.set (y, z)'");
                        }
                    }

                    if (line == "using GLib;") {
                        errs.add (@"Line $line_num : Useless reference to GLib");
                    }

                    if (math_re.match (line) && !("--" in line || "++" in line)) {
                        errs.add (@"Line $line_num : Math operators are not correctly spaced");
                    }

                    if (line.strip () == "{") {
                        errs.add (@"Line $line_num : use One True Brace Style");
                    }

                    if (after_meth_declaration && line.strip ().length == 0) {
                        errs.add (@"Line $line_num : never put an empty line after a method declaration");
                    }
                }
            } catch (Error err) {
                errs.add (err.message);
            }
            return errs;
        }

        /**
        * Lints your code with the GNOMEs guidelines.
        *
        * The guidelines can be found here: https://wiki.gnome.org/Projects/Vala/Hacking#Coding_Style
        *
        * @param source The code to lint
        * @return An {@link Gee.ArrayList} of {@link GLib.string} containing the errors
        */
        private ArrayList<string> gnome_lint (string source) {
            ArrayList<string> errs = new ArrayList<string> ();

            Regex par_re = /\w\(/; // to match parentheses with a non-empty character before

            int line_num = 0;
            foreach (string line in source.split ("\n")) {
                line_num++;
                if ("//" in line) {
                    errs.add (@"Line $line_num : Use /* C style comments */");
                }

                if ("  " in line) {
                    errs.add (@"Line $line_num : Use tabs for identation, rather than spaces");
                }

                if (par_re.match (line)) {
                    errs.add (@"Line $line_num : Missing space before parentheses");
                }
            }

            return errs;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            if (is_short) {
                return "Lints your code.";
            } else {
                return """Lints your code.
You can use elementary's guidelines, GNOME ones or both, using resectively the `--elementary', `--gnome' `--both' switches.
You can ignore the warning about properties using `--ignore-properties'.""";
            }
        }

    }
}
