using Gee;
using Poulp;

namespace Poulp.Commands {
    /**
    * A command that help you quickly creating files from templates.
    */
    public class GenCommand : Object, Command {

        /**
        * {@inheritDoc}
        */
        public async bool run (string[] args) {
            try {
                // we need at least the output path, --template and the tempate ID
                if (args.length < 3) {
                    Logger.log_error ("Usage : poulp gen [output] --template [template]");
                    return false;
                }
                // we need --template
                if (!("--template" in args)) {
                    Logger.log_error ("Please choose a template with `--template'");
                    return false;
                }
                // -- template can't be the last argument
                if (args[args.length - 1] == "--template") {
                    Logger.log_error ("Please specify a template");
                    return false;
                }

                // We get the output file and the template ID
                string template = "";
                string output = "";
                bool next_is_tmpl = false;
                foreach (string arg in args) {
                    if (arg == "--template") {
                        next_is_tmpl = true;
                        continue;
                    }

                    if (next_is_tmpl) {
                        template = arg;
                        next_is_tmpl = false;
                    } else {
                        output = arg;
                    }
                }

                // we read the `files.json` file to try to find a template corresponding to the one the user have choosen
                string files;
                FileUtils.get_contents (Path.build_filename (get_poulp_dir (), "files", "files.json"), out files);
                Json.Parser parser = new Json.Parser ();
                parser.load_from_data (files);

                bool tmpl_found = false;
                // we iterate over the different registered file templates
                foreach (var elt in parser.root.as_array ()) {
                    Json.Object tmpl = elt.as_object ();
                    // if the ID of the template or its name is the one we choose
                    if (tmpl["id"].as<string> () == template || tmpl["name"].as<string> () == template) {
                        // We copy the files of the template
                        foreach (var file in tmpl["files"].as_array ()) {
                            try {
                                File origin = File.new_for_path (Path.build_filename (get_poulp_dir (), "files", file.as<string> ()));
                                File dest = File.new_for_commandline_arg (output + "." + new ArrayList<string>.wrap (file.as<string> ().split (".")).last ());
                                origin.copy (dest, FileCopyFlags.OVERWRITE);
                            } catch (Error err) {
                                Logger.log_error (err.message);
                            }
                        }
                        tmpl_found = true;
                    }
                }

                if (!tmpl_found) {
                    Logger.log_error ("Can't find the specified template.");
                    return false;
                }
            } catch (Error err) {
                Logger.log_error (err.message);
            }

            return true;
        }

        /**
        * {@inheritDoc}
        */
        public string help (bool is_short) {
            if (is_short) {
                return "Creates one or many files from a template.";
            } else {
                return """
Creates one or many files from a template.

It looks for templates in %s%cfiles .
You can create new templates manually or install them with their libraries using poulp install""".printf (get_poulp_dir (), Path.DIR_SEPARATOR);
            }
        }

    }
}
