##
# Poulp Makefile.
# @license LGPLv3
##

SRCDIR = ./src
BUILDDIR = ./build

OUTPUT = $(BUILDDIR)/poulp
PKG = --pkg gee-0.8 --pkg gio-2.0 --pkg json-vala --pkg semver --pkg libsoup-2.4 --pkg toml-glib --pkg libpoulp
POULPFILES = $(SRCDIR)/*.vala $(SRCDIR)/commands/*.vala

.SILENT:

.PHONY: build

build:
	mkdir -p $(BUILDDIR)/
	echo Compiling with `valac --version`
	valac $(POULPFILES) $(PKG) --output=$(OUTPUT) -X -w --enable-deprecated
	echo Compilation ended

.ONESHELL:
install-linux:
	install -m 777 $(OUTPUT) $(PREFIX)/usr/bin/

lint:
	wget https://framagit.org/valse/vala-style/raw/master/style.rb
	ruby style.rb
	rm style.rb*

style:
	astyle \
		--style=attach \
		--indent=spaces=2 \
		--indent-namespaces \
		--indent-switches \
		--add-brackets \
		$(SRCDIR)/**.vala \
		$(SRCDIR)/*/**.vala

clean:
	rm -rf $(BUILDDIR)/
	rm -f $(SRCDIR)/*.orig
	rm -f $(SRCDIR)/*/*.orig
